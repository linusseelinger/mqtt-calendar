# mqtt-calendar

## load-caldav

### Requirements (python3)

- google_auth_oauthlib
- caldav
- pyyaml

### Google provider

1. Follow the instructions on
   [developers.google.com](https://developers.google.com/calendar/quickstart/python)
   to download credentials.json and activate app. (Enable caldav API, not
   calendar API)
2. Update your config file with some of your calendars.
3. Run `./load-caldav.py`, you will get a link in the console that will lead
   you to a Google OAuth dialog.

### Config file

tbd

## Qt

### Requirements (python3)

- pyside2
