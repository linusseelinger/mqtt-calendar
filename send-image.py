#!/usr/bin/env python3
import paho.mqtt.client as mqtt

client = mqtt.Client()

def on_connect(client, userdata, flags, rc):
  print("Connected with result code " + str(rc))
  f=open("image_test.jpg", "rb")
  client.publish("mqtt-epaper/TARGET-DEVICE-NAME/image", f.read())
  client.disconnect()

client.on_connect = on_connect

client.connect("127.0.0.1", 1883, 60)

client.loop_forever()
