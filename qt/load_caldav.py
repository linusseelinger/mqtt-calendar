#!/usr/bin/env python3
import pickle
import os.path
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import caldav
from requests.auth import AuthBase
import yaml
import sys
import requests.exceptions
import requests
from requests.auth import HTTPBasicAuth

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/calendar'] # for some reason caldav needs rw access...


class OAuth(AuthBase):
    def __init__(self, credentials):
        self.credentials = credentials

    def __call__(self, r):
        self.credentials.apply(r.headers)
        return r


def init_caldav():
    scriptdir = os.path.dirname(os.path.realpath(__file__))
    try:
        config = yaml.safe_load(open(os.path.join(scriptdir, "config.yml")))
    except FileNotFoundError:
        print("Could not find config file")
        sys.exit(1)

    calendars = []
    notetext = ""

    if not "calendars" in config:
        print("No calendar config found")
        sys.exit(1)

    try:
        for provider in config["calendars"]:
            if provider == "google":
                """Shows basic usage of the Google Calendar API.
                Prints the start and name of the next 10 events on the user's calendar.
                """
                creds = None
                # The file token.pickle stores the user's access and refresh tokens, and is
                # created automatically when the authorization flow completes for the first
                # time.
                if os.path.exists('token.pickle'):
                    with open('token.pickle', 'rb') as token:
                        creds = pickle.load(token)
                # If there are no (valid) credentials available, let the user log in.
                if not creds or not creds.valid:
                    if creds and creds.expired and creds.refresh_token:
                        creds.refresh(Request())
                    else:
                        flow = InstalledAppFlow.from_client_secrets_file(
                            'credentials.json', SCOPES)
                        creds = flow.run_local_server(port=0)
                    # Save the credentials for the next run
                    with open('token.pickle', 'wb') as token:
                        pickle.dump(creds, token)

                try:
                    for calid in config["calendars"][provider]:
                        url = "https://apidata.googleusercontent.com/caldav/v2/" + calid + "/events"
                        #print("url:", url)
                        client = caldav.DAVClient(url, auth=OAuth(creds))

                        for calendar in client.principal().calendars():
                            #print(calendar.name)
                            calendars.append(calendar)
                except TypeError:
                    print("No calendar for provider", provider)

            elif provider == "plain":
                try:
                    for url in config["calendars"][provider]:
                        try:
                            client = caldav.DAVClient(url)
                            for calendar in client.principal().calendars():
                                try:
                                    if calendar.name in config["calendars"][provider][url]:
                                        calendars.append(calendar)
                                except TypeError:
                                    calendars.append(calendar)
                        except requests.exceptions.ConnectionError:
                            print("Could not connect to calendar at", url)
                except TypeError:
                    print("No calendar for provider", provider)

            else:
                print("Unknown provider:", provider)
    except TypeError:
        print("No providers found")



    if "notes" in config:
      for provider in config["notes"]:
        r = requests.request(
            method='get',
            url=provider,
            auth=(config["notes"][provider]["user"], config["notes"][provider]["pwd"])
        )
        notetext += r.text


    return calendars, notetext

if __name__ == '__main__':
    init_caldav()
