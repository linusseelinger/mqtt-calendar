#!/usr/bin/env python3
import sys, os
import signal

from PySide2.QtGui import QGuiApplication, QFont
from PySide2.QtQml import QQmlApplicationEngine, qmlRegisterType
from PySide2.QtCore import QObject, Property, Signal, QUrl, QAbstractListModel, Qt, QModelIndex, QSortFilterProxyModel, Slot

import datetime
from load_caldav import init_caldav

class CalendarSortProxyModel(QSortFilterProxyModel):
    def __init__(self, parent=None):
        super(CalendarSortProxyModel, self).__init__(parent)

    def setSourceModel(self, model):
        super(CalendarSortProxyModel, self).setSourceModel(model)
        self.sourceModelChanged.emit()
        self.sort(0)

    def lessThan(self, left, right):
        # return self.sourceModel().modelData[left.row()]["date"] < self.sourceModel().modelData[right.row()]["date"]
        return self.sourceModel().modelData[left.row()]["time"] < self.sourceModel().modelData[right.row()]["time"]

class CalendarModel(QAbstractListModel):
    SummaryRole     = Qt.UserRole + 1
    LocationRole    = Qt.UserRole + 2
    TimeRole        = Qt.UserRole + 3
    EndTimeRole     = Qt.UserRole + 4
    DayEventRole    = Qt.UserRole + 5
    WeekdayRole     = Qt.UserRole + 6
    DateRole        = Qt.UserRole + 7

    def __init__(self, calendars, parent=None):
        super(CalendarModel, self).__init__(parent)
        self.modelData = []

        for calendar in calendars:
            startdate = datetime.date.today()
            for date in range(6):
                enddate = startdate + datetime.timedelta(days=1)
                events = calendar.date_search(startdate, enddate)
                print(f"<<< {len(events)} events in {calendar.name} on {startdate}:")
                for event in events:
                    e = event.instance.vevent
                    print(e)
                    eventDay = startdate.strftime("%a")
                    eventTime = e.dtstart.value.strftime("%H:%M")
                    eventEndTime = e.dtend.value.strftime("%H:%M")
                    eventSummary = e.summary.value
                    eventDate = startdate.strftime("%d.%m")
                    dayEvent = eventTime == "00:00" and eventEndTime == "00:00" and e.dtstart.value != e.dtend.value
                    try:
                        eventLocation = e.location.value
                    except:
                        eventLocation = ""
                    print("==================================")
                    print(f"Event:    {eventSummary}")
                    print(f"Time:     {eventDate} {eventTime}-{eventEndTime}{dayEvent} ({eventDay})")
                    print(f"Location: {eventLocation}")
                    self.modelData.append({
                        "summary": eventSummary,
                        "location": eventLocation,
                        "time": eventTime,
                        "endTime": eventEndTime,
                        "dayEvent": dayEvent,
                        "weekday": eventDay,
                        "date": eventDate
                    })
                startdate = enddate

        # self.modelData = [{"summary": "Dinner", "location": "Dining room", "time": "19:00", "weekday": "Mon"},
        #                   {"summary": "Save the world", "location": "Somewhere in the world", "time": "08:15", "weekday": "Wed"},
        #                   {"summary": "Eat chicken", "location": "Good restaurant", "time": "12:00", "weekday": "Fri"}]

    def roleNames(self):
        return {
            CalendarModel.SummaryRole: b"summary",
            CalendarModel.LocationRole: b"location",
            CalendarModel.TimeRole: b"time",
            CalendarModel.EndTimeRole: b"endTime",
            CalendarModel.DayEventRole: b"dayEvent",
            CalendarModel.WeekdayRole: b"weekday",
            CalendarModel.DateRole: b"date",
            Qt.DisplayRole:     b"display"
        }

    def rowCount(self, index=QModelIndex()):
        return len(self.modelData)

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return None

        if not 0 <= index.row() < len(self.modelData):
            return None

        data = self.modelData[index.row()]
        if role == CalendarModel.SummaryRole:
            return data["summary"]
        elif role == CalendarModel.LocationRole:
            return data["location"]
        elif role == CalendarModel.TimeRole:
            return data["time"]
        elif role == CalendarModel.EndTimeRole:
            return data["endTime"]
        elif role == CalendarModel.DayEventRole:
            return data["dayEvent"]
        elif role == CalendarModel.WeekdayRole:
            return data["weekday"]
        elif role == CalendarModel.DateRole:
            return data["date"]
        return None

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role == CalendarModel.SummaryRole:
            return "Name"
        elif role == CalendarModel.LocationRole:
            return "Data"
        elif role == CalendarModel.TimeRole:
            return "Time"
        elif role == CalendarModel.EndTimeRole:
            return "End Time"
        elif role == CalendarModel.DayEventRole:
            return "Day Event"
        elif role == CalendarModel.WeekdayRole:
            return "Day"
        elif role == CalendarModel.DateRole:
            return "Date"
        return None

    # def insertRows(self, position, rows=1, index=QModelIndex()):
    #     self.beginInsertRows(QModelIndex(), position, position + rows - 1)
    #
    #     for row in range(rows):
    #         self.modelData.insert(position + row, {"name":"", "data":""})
    #
    #     self.endInsertRows()
    #     return True
    #
    # def removeRows(self, position, rows=1, index=QModelIndex()):
    #     self.beginRemoveRows(QModelIndex(), position, position + rows - 1)
    #
    #     del self.modelData[position:position+rows]
    #
    #     self.endRemoveRows()
    #     return True

    # def setData(self, index, value, role=Qt.EditRole):
    #     if not index.isValid():
    #         return False
    #
    #     if not 0 <= index.row() < len(self.modelData):
    #         return False
    #
    #     if role == CalendarModel.NameRole:
    #         self.modelData[index.row()]["name"] = value
    #     elif role == CalendarModel.DataRole:
    #         self.modelData[index.row()]["data"] = value
    #     else:
    #         return False
    #
    #     self.dataChanged.emit(index, index)
    #     return True

    def flags(self, index):
        if not index.isValid():
            return Qt.ItemIsEnabled
        return Qt.ItemFlags(QAbstractTableModel.flags(self, index) |
                            Qt.ItemIsEditable)

if __name__ == "__main__":
    app = QGuiApplication(sys.argv)


    app.setFont(QFont("Arial",17))

    qmlRegisterType(CalendarSortProxyModel, 'CalendarSortProxyModel', 1, 0, 'CalendarSortProxyModel')

    calendars, notetext = init_caldav()
    calendarmodel = CalendarModel(calendars)

    engine = QQmlApplicationEngine()
    engine.rootContext().setContextProperty("calendarmodel", calendarmodel)
    engine.rootContext().setContextProperty("currentdate", datetime.datetime.now().strftime("%a %b %-d, %Y"))
    engine.rootContext().setContextProperty("notetext", notetext)

    main_qml = os.path.join(os.path.dirname(__file__), "main.qml")
    engine.load(QUrl(main_qml))

    if not engine.rootObjects():
        sys.exit(-1)

    # allow ctrl-c to exit the program
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    app.exec_()
