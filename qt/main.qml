import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import CalendarSortProxyModel 1.0

ApplicationWindow {
    id: window
    width: 800
    height: 480
    visible: true

    Item {
        anchors.fill: parent


        /*
         * TODO: Use QtGraphicalEffects to apply some effects.
         *
         * See https://doc.qt.io/qt-5/qtgraphicaleffects-qmlmodule.html for more
         * information.
         */
        Image {
            id: bg
            anchors.fill: parent
            source: "background.png"
            fillMode: Image.PreserveAspectCrop
        }

        Column {
            id: column
            anchors.margins: 10
            anchors.top: parent.top
            anchors.left: parent.left
            spacing: 5

            Repeater {
                model: {
                    let ret = []
                    let date = new Date()
                    for (let i = 0; i < 7;) {
                        let datestring = Qt.formatDate(date, "dd\\.MM")
                        ret.push(new RegExp(datestring))
                        date.setDate(new Date().getDate() + ++i)
                    }
                    return ret
                }

                Rectangle {
                    height: listview.height
                    width: listview.width

                    radius: 10
                    border.width: 1

                    ListView {

                        id: listview

                        height: contentHeight
                        width: contentWidth
                        contentWidth: window.width * 0.5

                        model: CalendarSortProxyModel {
                            sourceModel: calendarmodel
                            filterRole: Qt.UserRole + 7 //CalendarModel.DateRole
                            filterRegExp: modelData
                        }

                        delegate: Item {
                            anchors.left: parent.left
                            anchors.right: parent.right
                            height: calendarrow.height

                            RowLayout {
                                id: calendarrow
                                spacing: 2
                                anchors.left: parent.left
                                anchors.right: parent.right

                                Image {
                                    id: logo
                                    Layout.margins: 3
                                    Layout.preferredWidth: 35
                                    Layout.preferredHeight: 35
                                    source: "logo.png"
                                    fillMode: Image.PreserveAspectFit
                                }
                                Text {
                                    id: date
                                    antialiasing: false
                                    renderType: Text.NativeRendering
                                    Layout.preferredWidth: 60
                                    Layout.alignment: Qt.AlignVCenter
                                    text: model.date
                                    color: "black"
                                }
                                Text {
                                    id: weekday
                                    antialiasing: false
                                    renderType: Text.NativeRendering
                                    Layout.preferredWidth: 50
                                    Layout.alignment: Qt.AlignVCenter
                                    text: model.weekday
                                    color: "black"
                                }
                                Text {
                                    id: time
                                    antialiasing: false
                                    renderType: Text.NativeRendering
                                    Layout.preferredWidth: 70
                                    Layout.alignment: Qt.AlignVCenter
                                    text: (model.dayEvent) ? "" : model.time
                                    color: "black"
                                }

                                Item {
                                    Layout.alignment: Qt.AlignVCenter
                                    Layout.fillWidth: true
                                    Layout.preferredHeight: col.implicitHeight

                                    ColumnLayout {
                                        id: col
                                        anchors.left: parent.left
                                        anchors.right: parent.right

                                        Text {
                                            id: summary
                                            antialiasing: false
                                            renderType: Text.NativeRendering
                                            Layout.preferredWidth: parent.width
                                            text: model.summary
                                            color: "black"
                                            wrapMode: Text.Wrap // Max of 2 lines, with elision
                                            elide: Text.ElideRight
                                            maximumLineCount: 2
                                            visible: text !== ""
                                            //font: Qt.font({family:"Arial", pointSize: 12})
                                        }
                                        Text {
                                            id: location
                                            antialiasing: false
                                            renderType: Text.NativeRendering
                                            Layout.preferredWidth: parent.width
                                            text: model.location
                                            color: "black"
                                            wrapMode: Text.Wrap // Max of 2 lines, with elision
                                            elide: Text.ElideRight
                                            maximumLineCount: 2
                                            visible: text !== ""
                                        }
                                    } //ColumnLayout
                                } //Item
                            } //RowLayout
                        } //Item
                    } //ListView
                } //Rectangle
            } //Repeater
        } //Column


        Rectangle {
            id: currentdatebox
            anchors.margins: 10
            anchors.top: parent.top
            anchors.right: parent.right
            height: currentdatetext.height
            width: currentdatetext.width

            radius: 10
            border.width: 1
            Text {
                padding: 3
                id: currentdatetext
                //anchors: parent.fill
                antialiasing: false
                renderType: Text.NativeRendering
                Layout.preferredWidth: parent.width
                text: currentdate
            }
        }

        Rectangle {
            anchors.left: column.right
            anchors.top: currentdatebox.bottom
            anchors.right: parent.right
            anchors.margins: 10
            height: notestext.height
            visible: notetext != ""

            radius: 10
            border.width: 1
            Text {
                padding: 3
                id: notestext
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                //anchors: parent.fill
                antialiasing: false
                renderType: Text.NativeRendering
                Layout.preferredWidth: parent.width
                text: notetext
                wrapMode: Text.Wrap
            }
        }

        Component.onCompleted: {
            grabToImage(function(result) {
                result.saveToFile("capture.png");
            });
        }
    } //Item
}
