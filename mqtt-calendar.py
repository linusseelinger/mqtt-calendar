#!/usr/bin/env python3
from datetime import datetime, timedelta, time
import caldav

url = "https://linus:secretpasswd@nextcloudaddress/remote.php/dav/calendars/linus/"
today  = datetime.combine(datetime.today(), time(0,0))

client = caldav.DAVClient(url)
principal = client.principal()
calendars = principal.calendars()

def get_event_time(event):
  event.load()
  e = event.instance.vevent
  try:
    return e.dtstart.value.time()
  except:
    return time(0) # Some events (e.g. full-day events) have no time value; in that case, return a fallback

if len(calendars) > 0:

  for day in range(0,7): # Loop through events of the next days
    check_day = today+timedelta(days=day)
    check_day_str = check_day.strftime("%a")

    # Collect events from all desired calendars
    events = []
    for calendar in calendars:
      if calendar.name in ("Freizeit", "Persönlich"):
        events = events + calendar.date_search(check_day, datetime.combine(check_day, time(23,59,59,59)))
    events = sorted(events, key=get_event_time)

    if len(events) > 0:
      print("=============")
      print(f"{len(events)} events on {check_day_str}:")

      for event in events:
        event.load()
        e = event.instance.vevent
        eventDay = e.dtstart.value.strftime("%a")
        eventTime = e.dtstart.value.strftime("%H:%M")

        print (f"Event: {e.summary.value}")
        print (f"Time: {eventTime}")
        try:
          print (f"Location: {e.location.value}")
        except:
          pass

