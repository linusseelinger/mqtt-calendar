def get_mqtt_epaper_res(mqtt_broker_address, mqtt_topic):
    client = mqtt.Client()

    def on_connect(client, userdata, flags, rc):
      print("Connected with result code " + str(rc))

      client.subscribe(f"{mqtt_topic}/screen_width")
      client.subscribe(f"{mqtt_topic}/screen_height")

    width = 0
    height = 0
    def on_message(client, userdata, msg):
      nonlocal width, height
      if (msg.topic == f"{mqtt_topic}/screen_width"):
        width = int(msg.payload)
      if (msg.topic == f"{mqtt_topic}/screen_height"):
        height = int(msg.payload)

      if height != 0 and width != 0:
        client.disconnect()

    client.on_connect = on_connect
    client.on_message = on_message

    client.connect(mqtt_broker_address, 1883, 60)

    client.loop_forever()
    return [width, height]
